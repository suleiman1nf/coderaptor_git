﻿using System;

namespace Vehicle
{
    [Serializable]
    public class VehicleData
    {
        public string vehicleName;
        public string vehicleDescription;
    }
}