using System.Collections.Generic;
using UnityEngine;

namespace Vehicle
{
    public class VehicleRoot : MonoBehaviour
    {
        [SerializeField] private float length;
        [SerializeField] private VehicleOutLine vehicleOutLine;
        [SerializeField] private VehicleData vehicleData;
        private VehicleMove vehicleMove;
        private VehicleDestroyer vehicleDestroyer;
        public float Length => length;

        public VehicleMove VehicleMove => vehicleMove;

        public VehicleOutLine VehicleOutLine => vehicleOutLine;

        public VehicleData VehicleData => vehicleData;

        public void Init(List<Vector3> positions, Vector3 deathPosition, float speed)
        {
            vehicleMove = new VehicleMove(transform, positions, speed);
            vehicleDestroyer = new VehicleDestroyer(transform, deathPosition, length);
            vehicleMove.StartMove();
        }

        public void Update()
        {
            vehicleDestroyer?.Update(Time.deltaTime);
        }

        public void OnDestroy()
        {
            vehicleMove.OnDestroy();
        }
    }
}
