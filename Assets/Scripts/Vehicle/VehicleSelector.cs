﻿using Core;
using UnityEditor;
using UnityEngine;
using View;

namespace Vehicle
{
    public class VehicleSelector : ObjectSelector<VehicleRoot>
    {
        [SerializeField] private UI ui;
        [SerializeField] private FollowCamera followCamera;
        protected override void OnSelectObject(VehicleRoot obj)
        {
            obj.VehicleOutLine.Enable();
            followCamera.SetTarget(obj.transform);
            ui.InfoPanel.Show();
            ui.InfoPanel.ChangeInfo(obj.VehicleData.vehicleName, obj.VehicleData.vehicleDescription);
        }

        protected override void OnDeselectObject(VehicleRoot obj)
        {
            obj.VehicleOutLine.Disable();
        }

        protected override void OnSelectNothing()
        {
            followCamera.SetTarget(null);
            ui.InfoPanel.Hide();
        }
    }
}