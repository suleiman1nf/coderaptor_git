using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace Vehicle
{
    public class VehicleFabric : MonoBehaviour
    {
        [SerializeField] private VehicleRoot[] vehicles;
        public VehicleRoot[] Vehicles => vehicles;

        public VehicleRoot CreateVehicle(VehicleRoot prefab, Transform container)
        {
            if (vehicles.ToList().Exists((p) => prefab))
            {
                VehicleRoot vehicle = Instantiate(prefab, container);
                return vehicle;    
            }
            else
            {
                throw new ArgumentException();
            }
        }
    }
}
