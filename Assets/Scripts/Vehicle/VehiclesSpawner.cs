﻿using System.Collections.Generic;
using UnityEngine;

namespace Vehicle
{
    public class VehiclesSpawner
    {
        private readonly Transform vehiclesContainer;
        private List<VehicleRoot> vehicles = new List<VehicleRoot>();
        private readonly VehicleFabric vehicleFabric;
        
        public VehiclesSpawner(VehicleFabric vehicleFabric, Transform vehiclesContainer)
        {
            this.vehicleFabric = vehicleFabric;
            this.vehiclesContainer = vehiclesContainer;
        }
        
        public VehicleRoot SpawnVehicle(VehicleRoot prefab, Vector3 localPosition)
        {
            var vehicle = vehicleFabric.CreateVehicle(prefab, vehiclesContainer);
            vehicle.transform.localPosition = localPosition;
            vehicles.Add(vehicle);
            return vehicle;
        }
    }
}