using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Vehicle
{
    public class WheelRotator : MonoBehaviour
    {

        private float speed = 50;

        private void Update()
        {
            transform.Rotate(Vector3.right * (speed * Time.deltaTime), Space.Self);
        }
    }
}
