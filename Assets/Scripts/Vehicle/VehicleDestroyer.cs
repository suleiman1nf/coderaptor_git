﻿using UnityEngine;

namespace Vehicle
{
    public class VehicleDestroyer
    {
        private readonly Transform transform;
        private readonly Vector3 destination;
        private readonly float vehicleLength;

        public VehicleDestroyer(Transform transform, Vector3 destination, float vehicleLength)
        {
            this.transform = transform;
            this.vehicleLength = vehicleLength;
            this.destination = destination;
        }

        public void Update(float dt)
        {
            //check if transform position > destination by vector dot
            if (Vector3.Dot(transform.forward, transform.position - transform.forward * vehicleLength /2 - destination ) > 0)
            {
                Destroy();
            }
        }

        private void Destroy()
        {
            GameObject.Destroy(transform.gameObject);
        }
    }
}