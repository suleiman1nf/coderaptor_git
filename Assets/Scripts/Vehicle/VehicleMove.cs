using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Vehicle
{
    public class VehicleMove
    {
        private readonly float speed;
        private Tween tween;
        private readonly Transform transform;
        private readonly List<Vector3> positions;

        public VehicleMove(Transform transform, List<Vector3> positions, float speed)
        {
            this.speed = speed;
            this.transform = transform;
            this.positions = positions;
        }

        public void StartMove()
        {
            if (tween == null)
            {
                transform.position = positions[0];
                tween = transform.DOPath(positions.ToArray(), speed).SetSpeedBased(true).SetLookAt(0.01f).SetEase(Ease.Linear);    
            }
            else
            {
                tween.Play();
            }
        }

        public void StopMove()
        {
            tween.Pause();
        }

        public void OnDestroy()
        {
            tween.Kill();
        }
    }
}
