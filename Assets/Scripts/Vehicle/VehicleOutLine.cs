﻿using System;
using UnityEngine;

namespace Vehicle
{
    [Serializable]
    public class VehicleOutLine
    {
        [SerializeField] private Outline outline;
        public void Enable()
        {
            outline.enabled = true;
        }

        public void Disable()
        {
            outline.enabled = false;
        }
    }
}