using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utilities;
using Vehicle;
using Random = UnityEngine.Random;

namespace Core
{
    public class RoadManager : MonoBehaviour
    {
        [SerializeField] private VehicleFabric vehicleFabric;
        [SerializeField] private float offsetMin;
        [SerializeField] private float offsetMax;
        [SerializeField] private float trafficSpeed;
        [SerializeField] private Transform wayPointsParent;
        [SerializeField] private Transform vehiclesContainer;
        [SerializeField] private Transform deathPoint;
        private List<Transform> movementWayPoints = new List<Transform>();
        private VehicleRoot lastVehicle;
        private VehicleRoot nextVehiclePrefab;
        private float currOffset;
        private VehiclesSpawner vehiclesSpawner;

        private void Awake()
        {
            if (vehicleFabric == null)
                throw new NullReferenceException();
            if (vehiclesContainer == null)
                throw new NullReferenceException();
            if (wayPointsParent == null)
                throw new NullReferenceException();

            foreach (Transform wayPoint in wayPointsParent)
            {
                movementWayPoints.Add(wayPoint);   
            }

            if (movementWayPoints.Count <= 1)
                Debug.LogError("ERROR! MovementWayPoints count less than 2");
            
            vehiclesSpawner = new VehiclesSpawner(vehicleFabric, vehiclesContainer);
        }

        public void Start()
        {
            nextVehiclePrefab = ChooseRandomVehicle();
            SpawnVehicle();
        }

        public void Update()
        {
            if (CanCreateNewVehicle(nextVehiclePrefab.Length, currOffset))
            {
                SpawnVehicle();
            }
        }

        private void SpawnVehicle()
        {
            lastVehicle = vehiclesSpawner.SpawnVehicle(nextVehiclePrefab, Vector3.zero);
            lastVehicle.Init(GetMovementPositions(),deathPoint.position, trafficSpeed);
            currOffset = Random.Range(offsetMin, offsetMax);
            nextVehiclePrefab = ChooseRandomVehicle();
        }

        private bool CanCreateNewVehicle(float spawnVehicleSize, float offset)
        {
            Vector3 currPosition = lastVehicle.transform.position -
                                   transform.forward * (lastVehicle.Length / 2 + spawnVehicleSize / 2 + offset);
            return Vector3.Dot(currPosition - movementWayPoints[0].position, transform.forward) > 0;
        }

        private VehicleRoot ChooseRandomVehicle()
        {
            return vehicleFabric.Vehicles.GetRandomElement();
        }

        private List<Vector3> GetMovementPositions()
        {
            return movementWayPoints.Select(wayPoint => wayPoint.transform.position).ToList();
        }

        private void OnDrawGizmos()
        {
            if (wayPointsParent == null)
            {
                Debug.LogError("sign waypoints parent!");
                return;
            }
            Gizmos.color = Color.green;
            foreach (Transform wayPoint in wayPointsParent)
            {
                Gizmos.DrawCube(wayPoint.position, Vector3.one);   
            }
            Gizmos.color = Color.red;
            Gizmos.DrawCube(deathPoint.position, Vector3.one);
        }
    }
}
