using System;
using UnityEngine;

namespace Core
{
    public abstract class ObjectSelector<T> : MonoBehaviour where T : Component
    {
        [SerializeField] protected Camera cam;

        private T lastSelected;
        
        public void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        
                if (Physics.Raycast(ray, out hit)) {
                    Transform objectHit = hit.transform;
                    if (objectHit.TryGetComponent(out T obj))
                    {
                        Deselect(lastSelected);
                        lastSelected = obj;
                        Select(obj);
                    }
                    else
                    {
                        Deselect(lastSelected);
                        OnSelectNothing();
                    }
                }
                else
                {
                    Deselect(lastSelected);
                    OnSelectNothing();
                }    
            }
        }

        private void Select(T obj)
        {
            Deselect(lastSelected);
            lastSelected = obj;
            OnSelectObject(obj);
        }

        protected abstract void OnSelectObject(T obj);

        private void Deselect(T obj)
        {
            if(obj != null)
                OnDeselectObject(obj);
            lastSelected = null;
        }

        protected abstract void OnDeselectObject(T obj);

        protected virtual void OnSelectNothing()
        {
        }
    }
}
