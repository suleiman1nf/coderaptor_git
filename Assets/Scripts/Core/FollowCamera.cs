using System;
using UnityEngine;

namespace Core
{
    public class FollowCamera : MonoBehaviour
    {
        [SerializeField] private Camera cam;
        [SerializeField] private Vector3 offset;
        private Transform target;
        private float smoothTime = 0.3f;
        
        public void SetTarget(Transform target)
        {
            this.target = target;
        }
        
        private Vector3 velocity = Vector3.zero;

        private void LateUpdate()
        {
            if (target == null)
                return;
            
            Vector3 targetPosition = target.position + target.TransformDirection(offset);
            cam.transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
            
            transform.LookAt(target);
        }
    }
}
