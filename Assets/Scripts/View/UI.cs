using System;
using UnityEngine;

namespace View
{
    public class UI : MonoBehaviour
    {
        [SerializeField] private InfoPanel infoPanel;

        public InfoPanel InfoPanel => infoPanel;

        private void Awake()
        {
            if (infoPanel == null)
                throw new NullReferenceException();
        }
    }
}
