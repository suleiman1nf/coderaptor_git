using System;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace View
{
    [RequireComponent(typeof(CanvasGroup))]
    public class InfoPanel : MonoBehaviour
    {
        [SerializeField] private TMP_Text title;
        [SerializeField] private TMP_Text description;
        private CanvasGroup canvasGroup;

        public void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.alpha = 0;

            if (title == null)
                throw new NullReferenceException();
            if (description == null)
                throw new NullReferenceException();
        }

        public void ChangeInfo(string titleText, string descriptionText)
        {
            title.SetText(titleText);
            description.SetText(descriptionText);
        }

        public void Show()
        {
            canvasGroup.DOFade(1, 0.5f);
        }

        public void Hide()
        {
            canvasGroup.DOFade(0, 0.5f);   
        }
    }
}
