using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public class Utils : MonoBehaviour
    {
        
    }

    public static class ExtensionMethods
    {
        public static T GetRandomElement<T>(this List<T> list)
        {
            return list[UnityEngine.Random.Range(0, list.Count)];
        }
        
        public static T GetRandomElement<T>(this T[] list)
        {
            return list[UnityEngine.Random.Range(0, list.Length)];
        }
    }
}
